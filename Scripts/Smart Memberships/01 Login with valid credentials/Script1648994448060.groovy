import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

RunConfiguration.setWebDriverPreferencesProperty('args', ['window-size=1920,1080'])

WebUI.enableSmartWait()

WebUI.openBrowser((((('http://' + GlobalVariable.AuthUser) + ':') + GlobalVariable.AuthPassword) + '@') + GlobalVariable.BaseURL)

WebUI.navigateToUrl((((('http://' + GlobalVariable.AuthUser) + ':') + GlobalVariable.AuthPassword) + '@') + GlobalVariable.BaseURL)

WebUI.delay(3)

WebUI.navigateToUrl(GlobalVariable.BaseURL)

WebUI.waitForElementClickable(findTestObject('CORE/SmartFunnel/Login/SF-Login-inputEmailAdd'), 30)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('CORE/SmartFunnel/Login/SF-Login-inputEmailAdd'), GlobalVariable.BaseUsername)

WebUI.setEncryptedText(findTestObject('CORE/SmartFunnel/Login/SF-Login-inputPassword'), GlobalVariable.BasePassword)

WebUI.click(findTestObject('CORE/SmartFunnel/Login/SF-Login-button_SIGN IN'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('CORE/SmartFunnel/Login/SF-Login-h1_Funnel_Overview'), FailureHandling.OPTIONAL)

