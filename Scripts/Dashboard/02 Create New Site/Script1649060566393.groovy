import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.callTestCase(findTestCase('Dashboard/01 Dashboard Login With Valid Credentials'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Dashboard/Page_Kyvio Dashboard/Dashboard - Current Site Dropdown/Dashboard-DashboardPage-CurrentSite-Dropdown'))

WebUI.delay(2)

WebUI.click(findTestObject('Dashboard/Page_Kyvio Dashboard/Dashboard - Current Site Dropdown/Dashboard-CurrentSiteDropdown-CreateNewSite-Option'))

WebUI.delay(2)

WebUI.setText(findTestObject('Dashboard/Page_Kyvio Dashboard/Dashboard - Site Manager/Create New Site/Dashboard-CreateNewSite-SiteName-Textfield'), 
    'HamzaTest321')

WebUI.setText(findTestObject('Dashboard/Page_Kyvio Dashboard/Dashboard - Site Manager/Create New Site/Dashboard-CreateNewSite-SiteURL-Textfield'), 
    'HamzaTest321url')

WebUI.click(findTestObject('Dashboard/Page_Kyvio Dashboard/Dashboard - Site Manager/Create New Site/Dashboard-CreateNewSite-Published-Button'))

WebUI.delay(2)

WebUI.click(findTestObject('Dashboard/Page_Kyvio Dashboard/Dashboard - Site Manager/Create New Site/Dashboard-CreateNewSite-SaveSiteSettings-Button'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Dashboard/Page_Kyvio Dashboard/Dashboard - Site Manager/Create New Site/Dashboard-CreateNewSite-SiteSavedSuccessfully-Message'), 
    10)

WebUI.click(findTestObject('Dashboard/Dashboard-Editsite-Saved Successfully'))

