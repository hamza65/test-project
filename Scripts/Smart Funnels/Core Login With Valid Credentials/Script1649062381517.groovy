import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser((((('http://' + GlobalVariable.AuthUser) + ':') + GlobalVariable.AuthPassword) + '@') + GlobalVariable.BaseURL)

WebUI.navigateToUrl((((('http://' + GlobalVariable.AuthUser) + ':') + GlobalVariable.AuthPassword) + '@') + GlobalVariable.BaseURL)

WebUI.setText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_EMAIL ADDRESS_email'), 
    'hamzaQA')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_PASSWORD_password'), 
    'iT4HCJBKQy6H/pp96g0kfTy34gIutBwu')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_SIGN IN'))

