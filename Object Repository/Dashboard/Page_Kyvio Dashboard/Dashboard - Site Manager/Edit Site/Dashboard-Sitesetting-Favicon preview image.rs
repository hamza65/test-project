<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Dashboard-Sitesetting-Favicon preview image</name>
   <tag></tag>
   <elementGuidId>78c3e596-3936-423d-871c-882ad21269bf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[3]/main/div[2]/div[2]/div[2]/div[4]/div/div/div[1]/div[2]/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
