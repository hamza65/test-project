<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Dashboard-NewIntegration-DemioAPIKey-Textfield</name>
   <tag></tag>
   <elementGuidId>498cff46-1eef-4d90-bfea-535ad363ddc8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[3]/main/div[4]/div/div/div/div[2]/div[5]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
