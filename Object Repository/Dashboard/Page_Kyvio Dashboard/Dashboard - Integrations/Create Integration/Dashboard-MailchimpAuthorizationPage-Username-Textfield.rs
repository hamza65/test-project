<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Dashboard-MailchimpAuthorizationPage-Username-Textfield</name>
   <tag></tag>
   <elementGuidId>2a08c1aa-a9eb-4c9a-81a5-1d2d65e9390c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[2]/div[4]/div/form/fieldset/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
