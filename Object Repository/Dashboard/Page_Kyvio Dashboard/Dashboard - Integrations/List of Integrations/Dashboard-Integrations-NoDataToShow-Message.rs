<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Dashboard-Integrations-NoDataToShow-Message</name>
   <tag></tag>
   <elementGuidId>37ffa6bc-6fb3-413f-89e9-f60ab0769bad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[3]/main/div[3]/div[2]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
