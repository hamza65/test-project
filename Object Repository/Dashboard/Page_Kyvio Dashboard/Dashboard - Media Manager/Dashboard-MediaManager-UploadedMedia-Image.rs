<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Dashboard-MediaManager-UploadedMedia-Image</name>
   <tag></tag>
   <elementGuidId>6be5ab7f-f48b-4783-aa0d-e5f44fe8725e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[3]/main/div[3]/div[2]/span[1]/div[2]/div/div[3]/div/div/div/div[1]/div[1]/div/button/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
