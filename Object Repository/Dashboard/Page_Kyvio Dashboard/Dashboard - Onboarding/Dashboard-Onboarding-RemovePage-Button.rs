<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Dashboard-Onboarding-RemovePage-Button</name>
   <tag></tag>
   <elementGuidId>3de08e94-a46c-4d0e-98e0-90b89e616ab6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/main/div[2]/div/div/div/div/span[2]/div/div[2]/ul/div[3]/div[2]/ul/li[2]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>round-link custom-link icon-remove</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/main[1]/div[@class=&quot;page-two-sides onboarding&quot;]/div[@class=&quot;page-right&quot;]/div[@class=&quot;onboarding-content&quot;]/div[1]/div[@class=&quot;tab-content section-padding pt-5 pb-5&quot;]/span[2]/div[@class=&quot;tab-pane fade show active&quot;]/div[@class=&quot;sortable-list&quot;]/ul[1]/div[@class=&quot;form-group row&quot;]/div[@class=&quot;col-sm-2&quot;]/ul[@class=&quot;list-unstyled list-in-block mb-0 color-gray-dark font-xs&quot;]/li[2]/p[@class=&quot;round-link custom-link icon-remove&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/main/div[2]/div/div/div/div/span[2]/div/div[2]/ul/div[3]/div[2]/ul/li[2]/p</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='checkout page'])[3]/following::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='content page'])[4]/following::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+ ADD'])[1]/preceding::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/preceding::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/ul/li[2]/p</value>
   </webElementXpaths>
</WebElementEntity>
