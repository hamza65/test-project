<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Dashboard-Notification-Message close button</name>
   <tag></tag>
   <elementGuidId>fd483123-0491-4b5d-b777-4e1ecbc236a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[3]/main/div[2]/div/div[1]/div/div[3]/div/div/div/div[3]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
