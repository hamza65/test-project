<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Dashboard-YourProfile-SearchPreferredLanguage-Textfield</name>
   <tag></tag>
   <elementGuidId>d2279121-6d2c-4d12-8a2b-0e0e2837a816</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[3]/main/div[2]/div[2]/span[2]/div[2]/section/div[8]/div/div/div[2]/li/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
